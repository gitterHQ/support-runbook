# Gitter Support Runbook

Unifying processes for handling support requests for Gitter.im and the `gitter.im` homeserver.


## Authenticate Zendesk request

When you receive a Zendesk ticket that requires manipulating user data and rooms, you
should validate that the Zendesk requester has control of the Matrix account and/or has
power levels in the room in question.

### Verify control of a Matrix account:

 1. We can check the primary email for someones Matrix account on the `gitter.im` Synapse database
    - See https://gitlab.matrix.org/new-vector/internal/-/wikis/Support/Verification
 1. If you're looking for historical email association, you can [check the Gitter Mongo database](#access-historical-snapshot-of-the-gitter-mongo-database)
    - See [how to connect to the historical snapshot of the Gitter Mongo database](#connect-to-the-historical-snapshot-of-the-old-gitter-mongo-database), then use the following queries:
    - Lookup via email (use both queries to check):
       - `db.users.findOne({ emails: 'badger@gitter.im' })`
       - `db.identities.find({ email: 'badger@gitter.im' })`
    - Lookup via Gitter user ID: `db.users.findOne({ _id: ObjectId('5411caa4163965c9bc20400c') })`
    - Lookup via Gitter username: `db.users.findOne({ username: 'gitter-badger' })`
    - Lookup backing social identity association for a given user: `db.identities.findOne({ userId: db.users.findOne({ username: 'gitter-badger' })._id })`
 1. Perhaps in the future, we can verify that they control the account in question if they message `@gitter-badger:gitter.im` with a link to the Zendesk ticket.
    - TODO: Can we have a bot/script to check this status?
    - Although ["we usually don't consider only having access to an active session on a
      account sufficient for account owner
      verification"](https://gitlab.matrix.org/new-vector/internal/-/wikis/Support/Verification)
      because TODO (perhaps because someone can hack one session while the rest remain
      under the true account holders control. Although it seems like someone could just
      change the email if they have one session so idk).

If things aren't lining up and it appears like they don't have the necessary information to control the account, you can respond with this copy-pasta:

```
Hey there,

I don't see a Gitter account associated with your email.

What is your Matrix ID or Gitter username? What social sign-in option are you using (GitLab, GitHub, Twitter)?
```

Of course you can grant mercy to anyone having trouble coming up with solid verification
evidence if you don't suspect any wrong-doing and they have no possible way to
self-serve (by themself or any others in their community) the thing they're trying to
do. It's best not to do any non-reversisable actions though (a rename or making
something private is fine since that can always be undone).


### Verify control of a Matrix room:

 1. Verify control of the Matrix account in question using the steps above
 1. Verify that the Matrix account has admin permissions in the Matrix room

If they do not have the proper power-levels in the room, you can respond with this
copy-pasta:

```
TODO
```

## Accessing Gitter data

`@gitter-badger:gitter.im` is a admin user of all Gitter rooms with power level of `100` created before/during the migration.

The Mjolnir `@moderation:gitter.im` also has `100` power level in all rooms created before/during the migration. Mjolnir can be controlled in the [**Moderation Gitter**](https://matrix.to/#/#gitter-moderation:matrix.org) room on Matrix.


### Connect to the `gitter.im` homeserver Synapse Postgres database

Instructions derived from https://gitlab.matrix.org/new-vector/internal/-/wikis/EMS-postgres#connecting-to-an-ems-database

(these instructions may be outdated since the EMS EKS migration)

 1. `ssh osiris.i.modular.im` (make sure to update your `~/.ssh/config` as [described in the internal wiki](https://gitlab.matrix.org/new-vector/internal/-/wikis/matrix-hosted#customer-workloads))
 1. `sudo -u kops -i bash`
 1. `ems-db-debug gitter.im`


## Access historical snapshot of the Gitter Mongo database 

 1. Connect to [Tailscale](https://gitlab.matrix.org/new-vector/internal/-/wikis/Operations/Tailscale)
 1. Get databse username and password from the `Gitter MongoDB Archive RO User` Passbolt entry (shared with Gitter and EMS support teams)
 1. `mongosh gitter-mongo-archive.vpn.infra.eu-west-2.staging.ems.element.dev --username <username> --password <password>`
 1. At the `>` Mongo prompt, type `use gitter` to select the relevant database
 1. You can now query the database as expected with any of the queries listed in this document
    - ex.: `db.users.findOne({ username: 'MadLittleMods' })`


## Support tasks

As a general rule of the thumb, we don't handle any requests that people can
self-service or someone else in their room/community can accomplish.


### People asking for general tech support

You can respond with the following copy-pasta. If you know of the appropriate room and
you want to spend the effort to get the `matrix.to` permalink, it would be nice to link
to the correct place to ask. If you see that they have already sent a message there,
remind them to be patient.

```
This is support for Gitter, the chat app itself, not general technical help.

If there is a room for the library/project you are using on Gitter, you can use that room instead.
```

```
This is support for Gitter, the chat app itself, not general technical help.

It looks like you're trying to get support with xxx, you can try asking in the `matrix.to/xxx` room.
```

### People asking about the ugly ID in the middle of their Matrix user ID (MXID)

If the user had an existing Gitter account [before the Matrix
migration](https://blog.gitter.im/2023/02/13/gitter-has-fully-migrated-to-matrix/#how-do-i-get-rid-of-this-ugly-id-in-the-middle-of-my-matrix-user-id-mxid/),
they're stuck with an ugly looking Matrix user ID (MXID) since there isn’t a way to
rename them and we wanted the user to have control of the same bridged user that had
been sending messages on Matrix.

ex. `@erictroupetester-55b214df0fc9f982beaad275:gitter.im` (see the [section below breaking down the different parts of a Gitter MXID](#gitter-mxid-breakdown))

You can respond to them with this copy-pasta:

```
Hey there,

If you had an existing Gitter account, sorry, you’re stuck with an ugly looking Matrix user ID (MXID) since there isn’t a way to rename them and we wanted you to have control of the same bridged user that has been sending messages on Matrix.

If you want a better MXID, you will need to create a new account and manually migrate your membership and permissions to that new account. It's not possible to use the same social login to create a new account as it can only be associated to a single Matrix account ([related issue](https://github.com/matrix-org/synapse/issues/9441#issuecomment-782110161)) and the [SSO assocation isn't removed on account deactivation](https://github.com/matrix-org/synapse/issues/11072).
```


#### Gitter MXID breakdown

Gitter MXID's created before/during the migration consist of the the Gitter username and their Gitter user ID separated by a dash.

GitHub example:

 - `@erictroupetester-55b214df0fc9f982beaad275:gitter.im`
    - `erictroupetester`: GitHub username
    - `55b214df0fc9f982beaad275`: Gitter user ID

GitLab example:

 - `@erictroupetester_gitlab-55b214df0fc9f982beaad275:gitter.im`
    - `erictroupetester_gitlab`: GitLab username with a `_gitlab` suffix to avoid name collisions with GitHub users
    - `55b214df0fc9f982beaad275`: Gitter user ID

Twitter example:

 - `@erictroupetester_twitter-55b214df0fc9f982beaad275:gitter.im`
    - `erictroupetester_twitter`: Twitter username with a `_twitter` suffix to avoid name collisions with GitHub users
    - `55b214df0fc9f982beaad275`: Gitter user ID

It's also possible to encounter some "ghost" users which just means that they [deleted and disassocated their personal information on Gitter which we called "ghosting"](https://gitlab.com/gitterHQ/webapp/-/blob/develop/docs/accounts.md#ghost-user).

Accounts created after the Matrix migration simply have whatever they set their username to. There is no correlation between their MXID and their SSO/social association.


### People asking about only having "Mod" role/permissions or power-level `90`

You can respond with this copy-pasta:

```
Hey there,

Perhaps this clarifies it for you: https://blog.gitter.im/2023/02/13/gitter-has-fully-migrated-to-matrix/#everyone-shows-up-as-mod-instead-of-admin-in-my-room
```


### Grant admin power-levels to a user

As a result of the historical Gitter import process and membership/permissions sync
process, some Matrix rooms were left with no room admins (only
`@gitter-badger:gitter.im` in the room with power levels). This could have been caused
their GitHub/GitLab token no longer being valid or their GitHub org had organization
access control that blocked Gitter at the time that we did the admin check.

If there are other admins in the Matrix room, we should point the user to rely on the
existing admins in the room to re-grant permissions to them. You can respond with this
copy-pasta:

```
It looks like there are already other admins in the room who can grant you the necessary permissions. You should reach out to them to accomplish this.
```

Otherwise, if there are no other admins in the room, it would be nice to help them out
so they can self-serve into the future without us.

 1. TODO: Is there any more straight-forward way that EMS support does this?

Manual steps:

 1. `GET https://gitter.ems.host/_matrix/client/r0/rooms/:roomId/state/m.room.power_levels`
    - Get access token from the `Gitter bridge application service token (as_token)` Passbolt entry (shared with Gitter and EMS support teams)
 1. Take the response and add the MXID of the user to the `users` object with a `90` value
 1. `PUT https://gitter.ems.host/_matrix/client/r0/rooms/:roomId/state/m.room.power_levels` with the JSON body from the previous step


### Shutting down a Matrix room (deleting)

You can respond with this copy-pasta:

```md
It's not possible to delete a Matrix room per-se but the equivalent result in Matrix is:

 1. Change history visibility so future people can't read the room
    - In Element: **Room Settings** -> **Security & Privacy** -> **Members only (since they joined)**
 1. Changing the join rules to invite only so people can't join back on their own
    - In Element: **Room Settings** -> **Security & Privacy** -> **Private (invite only)**
 1. Remove the room from the room directory
    - In Element: **Room Settings** -> **General** -> **Room Addresses** -> Disable the **Publish this room to the public in matrix.org's room directory?** option
 1. Removing all room aliases
    - In Element: **Room Settings** -> **General** -> **Room Addresses** -> remove any addresses/aliases listed under the **Published addresses** and **Local addresses**
 1. Kicking everyone out
    - In Element: There isn't a bulk tool for this. But you can go through the membership to **Remove from room** or use the `/remove @user` slash command
    - You won't be able to remove the `@gitter-badger:gitter.im` user since it has a higher power level
    - Finally, after clearing everyone else out, leave the room yourself via the room drop down context menu -> **Leave**
```

TODO: steps/scripts to actually make this happen. Maybe ask Trust & Safety how they accomplish this task?

Synapse has a [`DELETE /_synapse/admin/v1/rooms/<room_id>`](https://matrix-org.github.io/synapse/v1.41/admin_api/rooms.html#delete-room-api) API that probably works for this.


### Making a Matrix room private

 1. TODO: Is there any more straight-forward way that EMS support does this?

Manual steps:

 1. `PUT https://gitter.ems.host/_matrix/client/r0/rooms/:roomId/state/m.room.join_rules` with `{ "join_rule": "invite" }`
    - Get access token from the `Gitter bridge application service token (as_token)` Passbolt entry (shared with Gitter and EMS support teams)
 1. Check if there is a published room alias in the room directory: `GET https://gitter.ems.host/_matrix/client/r0/rooms/:roomId/state/m.room.canonical_alias`
    - Check for other local room aliases: `GET https://gitter.ems.host/_matrix/client/r0/rooms/:roomId//aliases`
 1. Remove the alias from the room directory (if any): `DELETE https://gitter.ems.host/_matrix/client/r0/directory/room/:roomAlias`


### GDPR request to clear personal information from their account

You can respond with this copy-pasta for them to self-service:

```
Thank you for getting in touch. If you wish to erase your data, you can do so by:

 1. Signing in at https://app.gitter.im/
 1. Click on your avatar/username in the top left corner -> **Settings**
 1. Scroll to the bottom of the **General** tab
 1. Click **Deactivate Account** -> check the option to "Hide my messages from new joiners", enter your password, and click **Continue**

Please note once the account has been deactivated, it is impossible to reactivate it again or reuse the username.
```

General account settings | Deactivate account dialog
--- | ---
![](/uploads/8296bc6d081d5ae6486e63be9e89d3c7/2023-05-15_22-00.png) | ![](/uploads/fcff56d5a5c34d42dff1cc5550c00d5c/2023-05-15_22-00_1.png)

If they don't have their password and don't have an email attached to the account:

```
As per recital 64 of the GDPR, we need to establish, through reasonable measures, the identity of a user before being able to perform a data erasure, predicted in article 17 of the same legislation.

Considering this account does not have any identifiers (i.e., email address or phone number) associated with it, we do not have a way to validate your identity and ownership of this account. This means we are unable to go ahead with your request for deletion.

If you wish to create a new account, we strongly recommend you associate an email address with it to prevent this situation from happening again.
```

If someone asks about their avatar/display name still showing up in the room history, you can respond with this copy-pasta:

```md
Due to the way the Matrix protocol works, avatar and display name changes live in the room directly as historical membership events (just like messages) separate from your profile and we currently don't have the tooling necessary to redact all of those historical membership events.

We are aware of required changes to the codebase of Synapse to enable this data to be erased. The progress of this work can be followed in this [matrix-org/synapse#15355](https://github.com/matrix-org/synapse/issues/15355). Once it is possible in Synapse, we will queue your erasure to be addressed.

Alternatively, if it is absolutely crucial for you to have this data removed now there may be the possibility of puppetting the old account and removing the events directly. This would fall under our Exceptional Erasure Request and it is something we avoid doing unless there are tangible risks for the individual if this data does not get erased.

Please do advise us on what course of action you would like us to take.
```



### Someone deactivated their account and is no longer able to sign-in with that SSO method

Since deactivating an account on Synapse, does not remove the SSO association (see
https://github.com/matrix-org/synapse/issues/11072), this means that the user will not
be able to sign-in with that SSO method again.

It's probably possible to allow them to sign-in again by removing the SSO association as
documented below.

Re-activating an account is not something Element support has historically done though.
Perhaps because deactivation can also come from a moderation action and we don't want to
let the person back in.

We can send the person, this copy-pasta explaining the situation:

```
Hey there,

Deactivating an account currently doesn't remove the SSO association and is tracked by https://github.com/matrix-org/synapse/issues/11072. This means you won't be able to use the account you deactivated or create another account with that same social login provider.

We also don't support account reactivation.

The best course of action is to use another social login provider on `gitter.im` or use another server like `matrix.org` which also supports SSO to create another account.
```


### Removing SSO association from an existing Matrix account

Probably possible with some Synapse rejiggering but not it's not something we support.

See https://github.com/matrix-org/synapse/issues/11072
